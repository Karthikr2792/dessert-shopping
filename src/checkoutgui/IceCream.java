/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkoutgui;

/**
 *
 * @author Karthik
 */
public class IceCream extends DessertItem{
    public int price;
   
     public IceCream(String name, int price)
    {
        super(name);
        this.price = price;
    }

    @Override
    public int getCost() {
        return price;    
    }
}
