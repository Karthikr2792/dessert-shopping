/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkoutgui;

/**
 *
 * @author Karthik
 */
public class Sundae extends IceCream{
    public String topping_name;
    public int topping_cost;
    public Sundae(String name, int price, String topping_name, int topping_cost)
    {
        super(name, price);
        this.topping_name = topping_name;
        this.topping_cost = topping_cost;
    }
    
    public int getCost() {
        return price + this.topping_cost;
    }
}
