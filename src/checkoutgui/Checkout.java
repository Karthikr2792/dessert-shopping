/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkoutgui;

import java.util.Formatter;
import java.util.Vector;

/**
 *
 * @author Karthik
 */
public class Checkout {
    
    static Vector<DessertItem> items = new Vector(50, 5);
    static int count;
    static int totalCost;
    static int totalTax;
    Checkout()
    {
        count = 0;
        totalCost = 0;
        totalTax = 0;
    }
    public static void enterItem(DessertItem c)
    {
        items.addElement(c);
        count++;
    }
    
    public static int numberOfItems()
    {
        return count;        
    }

    public int totalCost() {
        totalCost = 0;
        for(DessertItem d : items)
        {
            totalCost += d.getCost();
        }
        return totalCost;
    }
    
    public static void clear()
    {
        Checkout.items.clear();
        count = 0;
        Checkout.totalCost = 0;
        Checkout.totalTax = 0;
    }
    
    public static int totalTax()
    {
        Checkout.totalTax = (int) (Checkout.totalCost*DessertShoppe.TAX_RATE/100);
        return totalTax;
    }
    
    /**
     *
     * @return
     */
    public java.lang.String toString()
    {
        java.lang.String text = "";
        text = text+"\t\t"+DessertShoppe.STORE_NAME+"\n\t\t--------------------\n";
        (DessertItem d: items)
        {
            if(d instanceof Candy)
            {
               
                Candy a = (Candy) d;                
                text = text+"\n"+a.weightInPounds+" lbs. @ "+DessertShoppe.cents2dollarsAndCents(a.price)+"\n"+a.name+String.format("%30s", DessertShoppe.cents2dollarsAndCents(d.getCost()));
            }
            else if(d instanceof Cookie)
            {
                 Cookie a = (Cookie) d;                
                 long dozen = Math.round(a.numberInDozens);
                 text = text+"\n"+dozen + " cookies @ "+DessertShoppe.cents2dollarsAndCents(a.price)+"/dozen\n"+a.name+String.format("%30s", DessertShoppe.cents2dollarsAndCents(d.getCost()));
            }
            else if(d instanceof Sundae)
            {
                Sundae a = (Sundae) d;
                text = text + "\n" +a.topping_name+" Sundae with\n"+d.name + String.format("%30s", DessertShoppe.cents2dollarsAndCents(d.getCost())); 
            }
           else
            {
                text = text + "\n"+d.name+String.format("%30s", DessertShoppe.cents2dollarsAndCents(d.getCost()));
            }
            }
            this.totalCost();
            text = text + "\nTax"+String.format("%30s", DessertShoppe.cents2dollarsAndCents(this.totalTax()));
            int tc = this.totalCost +this.totalTax();
            text = text + "\nTotalCost"+ String.format("%24s", DessertShoppe.cents2dollarsAndCents(this.totalTax()+ this.totalCost));
            
        return text;
    }
}
