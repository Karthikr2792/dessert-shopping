/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkoutgui;

/**
 *
 * @author Karthik
 */
public class Candy extends DessertItem{
    public Double weightInPounds;
    public int price;
    
    public Candy(String name, Double weight, int price)
    {
        super(name);
        this.weightInPounds = weight;
        this.price = price;
        
    }
    

    @Override
    public int getCost() {
        int cost = (int)Math.round(this.weightInPounds*this.price);
        
        return cost;
    }
    
}
