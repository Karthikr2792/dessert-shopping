/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkoutgui;

/**
 *
 * @author Karthik
 */
public class Cookie extends DessertItem{
    public int numberInDozens;
    public int price;
    
     public Cookie(String name, int numberInDozens, int price)
    {
        super(name);
        this.numberInDozens = numberInDozens;
        this.price = price;
    }

    public int getCost() {
        int cost = (int)Math.round(this.numberInDozens*this.price/12);
        return cost;
    }
}
